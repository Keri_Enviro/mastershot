﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(menuName = "Configs/GameConfig")]
public class GameConfig : ScriptableObject
{
    public LevelPositions[] _LevelPositions = new LevelPositions[0];

    public void AddLevelPositions(List<Vector3> pPositions)
    {
        Vector3[] positions = pPositions.ToArray();

        int length = _LevelPositions.Length;
        LevelPositions[] oldArray = _LevelPositions;
        _LevelPositions = new LevelPositions[length + 1];
        for (int i = 0; i < length; i++)
        {
            _LevelPositions[i] = oldArray[i];
        }

        LevelPositions newPositions = new LevelPositions();
        newPositions._Positions = positions;
        _LevelPositions[length] = newPositions;
    }
}

[Serializable]
public struct LevelPositions
{
    public Vector3[] _Positions;
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameConfig))]
public class GameConfigEditor : Editor
{
    private int _Index = 0;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameConfig config = (GameConfig)target;

        //EditorGUILayout.Slider(_Index, 0, 10);
        //EditorGUILayout.IntSlider(_Index, 0, 10, new GUILayoutOption[0]);

        if (GUILayout.Button("Find all targets"))
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Target");
            List<Vector3> list = new List<Vector3>();
            foreach (GameObject go in gos)
            {
                if (go.activeSelf)
                {
                    Vector3 targetPos = go.transform.position;
                    list.Add(targetPos);
                }
            }

            config.AddLevelPositions(list);
        }
    }
}
#endif