﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _Force = 50f;

    private Rigidbody _RigiBody = null;

    private void Awake()
    {
        _RigiBody = GetComponent<Rigidbody>();
    }

    public void Shot(Vector3 pDirection, float pRelativeForce)
    {
        _RigiBody.AddForce(pDirection * (_Force * pRelativeForce), ForceMode.Impulse);
    }
}
