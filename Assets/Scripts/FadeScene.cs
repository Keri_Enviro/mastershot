﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScene : MonoBehaviour
{
    private static FadeScene _Instance = null;

    public static FadeScene Get()
    {
        return _Instance;
    }

    public const float  _EndTime = 0.5f;
    public const float  _EndTimeMainLevel = 1f;
    public CanvasGroup  _FadeScreen = null;
    public Text         _ProgressValue = null;
    public Image        _Progress = null;
    private bool        _IsFading = false;

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Debug.LogError("FadeScene is already on gameobject: " + gameObject);
        }

        GameRoot.Get().SceneController._OnLevelLoadingProgressChanged += OnLevelLoadingProgressChanged;
    }

    private void OnDestroy()
    {
        GameRoot.Get().SceneController._OnLevelLoadingProgressChanged -= OnLevelLoadingProgressChanged;
    }

    private void OnLevelLoadingProgressChanged(float pProgress)
    {
        _Progress.fillAmount = pProgress;
        _ProgressValue.text = (pProgress * 100f).ToString("F0") + "%";
    }

    public IEnumerator OpenFadeScene()
    {
        yield return StartCoroutine(FadeIn());
    }

    public IEnumerator CloseFadeScene(float pEndTime)
    {
        yield return StartCoroutine(FadeOut(pEndTime));
    }

    private IEnumerator FadeOut(float pEndTime)
    {
        while (_IsFading)
            yield return null;

        _IsFading = true;
        _FadeScreen.alpha = 1f;
        float time = 0f;
        float endTime = pEndTime;
        while (true)
        {
            time += Time.unscaledDeltaTime;
            _FadeScreen.alpha = 1f - (time / endTime);
            if (time > endTime)
            {
                _FadeScreen.alpha = 0f;
                break;
            }
            yield return null;
        }

        _IsFading = false;
    }

    private IEnumerator FadeIn()
    {
        while (_IsFading)
            yield return null;

        _IsFading = true;

        _FadeScreen.alpha = 0f;
        float time = 0f;
        float endTime = _EndTime;
        while (true)
        {
            time += Time.unscaledDeltaTime;
            _FadeScreen.alpha = time / endTime;
            if (time > endTime)
            {
                _FadeScreen.alpha = 1f;
                break;
            }
            yield return null;
        }

        _IsFading = false;
    }
}
