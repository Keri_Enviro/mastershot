﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public int              _ReachedLevel = 1;
    public List<LevelScore> _LevelScores = new List<LevelScore>();
}

[Serializable]
public struct LevelScore
{
    public float    _Time;
    public int      _Bullets;
}
