﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    [SerializeField] private AudioSource    _AudioSource = null;
    [SerializeField] private AudioClip      _MainMenuClip = null;
    [SerializeField] private AudioClip      _MainLevelClip = null;
    [SerializeField] private AudioClip      _EndClip = null;
    [SerializeField] private AudioMixer     _AudioMixer = null;

    public void PlayMusic(MusicType pMusic)
    {
        _AudioSource.Stop();

        switch (pMusic)
        {
            case MusicType.MainMenu:
                _AudioSource.clip = _MainMenuClip;
                break;
            case MusicType.MainLevel:
                _AudioSource.clip = _MainLevelClip;
                break;
            case MusicType.End:
                _AudioSource.clip = _EndClip;
                break;
        }

        _AudioSource.Play();
    }

    public void SetMasterVolume(float pVolume)
    {
        _AudioMixer.SetFloat("MasterVolume", pVolume);
    }

    public void SetMusicVolume(float pVolume)
    {
        _AudioMixer.SetFloat("MusicVolume", pVolume);
    }

    public void SetSoundEffectsVolume(float pVolume)
    {
        _AudioMixer.SetFloat("SoundEffectsVolume", pVolume);
    }

    public float GetMasterVolume()
    {
        float volume;
        _AudioMixer.GetFloat("MasterVolume", out volume);
        return volume;
    }

    public float GetMusicVolume()
    {
        float volume;
        _AudioMixer.GetFloat("MusicVolume", out volume);
        return volume;
    }

    public float GetSoundEffectsVolume()
    {
        float volume;
        _AudioMixer.GetFloat("SoundEffectsVolume", out volume);
        return volume;
    }

    public void SetSnapshot(GameState pState)
    {
        string snapShotName = "";
        switch (pState)
        {
            case GameState.Start:
            case GameState.Playing:
            case GameState.End:
                snapShotName = "FullVolume";
                break;
            case GameState.Paused:
                snapShotName = "PausedVolume";
                break;
        }

        AudioMixerSnapshot snapShot = _AudioMixer.FindSnapshot(snapShotName);
        _AudioMixer.TransitionToSnapshots(
            new AudioMixerSnapshot[]{ snapShot }, 
            new float[]{1f}, 
            0.5f);
    }
}
