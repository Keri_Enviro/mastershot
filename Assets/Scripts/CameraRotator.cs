﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    private float       _RotationY = 0f;
    private const float ROTATION_LIMIT = 60f;

    private void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float horizontal = Input.GetAxis("Horizontal");

        if (mouseX > 0.01f || mouseX < -0.01f)
        {
            _RotationY += mouseX;
        }

        if (horizontal > 0.01f || horizontal < -0.01f)
        {
            _RotationY += horizontal;
        }

        if (_RotationY > ROTATION_LIMIT)
        {
            _RotationY = ROTATION_LIMIT;
        }
        else if (_RotationY < -ROTATION_LIMIT)
        {
            _RotationY = -ROTATION_LIMIT;
        }

        transform.rotation = Quaternion.Euler(
            transform.rotation.eulerAngles.x,
            _RotationY,
            transform.rotation.eulerAngles.z
            );
    }
}
