﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    private Rigidbody   _Rigidbody = null;
    private Collider    _Collider = null;
    private AudioSource _AudioSource = null;

    private Vector3     _StartPosition = Vector3.zero;
    private Quaternion  _StartRotation = Quaternion.identity;

    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        _Collider = GetComponent<Collider>();
        _AudioSource = GetComponent<AudioSource>();

        _StartPosition = transform.position;
        _StartRotation = transform.rotation;
    }

    private void Start()
    {
        GameController.Instance.ScoreController.IncreaseMaxScore();
        GameController.Instance.ScoreController.OnMaxScoreReached += OnMaxScoreReached;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!_Rigidbody.useGravity)
        {
            _Rigidbody.useGravity = true;
            GameController.Instance.ScoreController.IncreaseScore();
        }

        if (GameController.Instance.GameState == GameState.Playing && transform.position.y > 2)
        {
            _AudioSource.pitch = Random.Range(0.95f, 1.05f);
            _AudioSource.Play();
        }
    }

    private void OnMaxScoreReached()
    {
        StartCoroutine(GoBack());
    }

    private IEnumerator GoBack()
    {
        yield return new WaitForSeconds(3);

        _Rigidbody.velocity = Vector3.zero;
        _Rigidbody.useGravity = false;
        _Collider.enabled = false;

        float time = 2f;
        float currentTime = 0f;

        Vector3 currentPositon = transform.position;
        Quaternion currentRotation = transform.rotation;

        while (true)
        {
            currentTime += Time.deltaTime;

            float lerpTime = currentTime / time;
            transform.position = Vector3.Lerp(currentPositon, _StartPosition, lerpTime);
            transform.rotation = Quaternion.Lerp(currentRotation, _StartRotation, lerpTime);

            if (currentTime > time)
            {
                break;
            }

            yield return null;
        }

        transform.position = _StartPosition;
        transform.rotation = _StartRotation;
        _Rigidbody.velocity = Vector3.zero;
        _Collider.enabled = true;
    }
}
