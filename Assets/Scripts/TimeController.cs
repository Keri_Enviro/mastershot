﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController
{
    public Action<float> OnTimeChanged;
    public Action<float> OnCountdownChanged;

    private float   _CurrentTime = 0f;
    private bool    _MeasureTime = false;

    public void Init()
    {
        _CurrentTime = 0f;
        GameController.Instance.OnGameStateChanged += OnGameStateChanged;
    }

    public void StartMeasuring()
    {
        _MeasureTime = true;
    }

    public void StopMeasuring()
    {
        _MeasureTime = false;
    }

    public void UpdateTC()
    {
        if (!_MeasureTime) return;

        _CurrentTime += Time.deltaTime;

        if (OnTimeChanged != null)
        {
            OnTimeChanged(_CurrentTime);
        }
    }

    public void StartCountdown()
    {
        GameController.Instance.StartCoroutine(StartCountdownCoroutine());
    }

    private void OnGameStateChanged(GameState pState)
    {
        switch (pState)
        {
            case GameState.Playing:
                Time.timeScale = 1f;
                break;
            case GameState.Paused:
                Time.timeScale = 0f;
                break;
        }
    }

    private IEnumerator StartCountdownCoroutine()
    {
        float currentTime = 3f;
        while (true)
        {
            currentTime -= Time.deltaTime;

            if (OnCountdownChanged != null)
            {
                OnCountdownChanged(currentTime);
            }

            if (currentTime < 0f)
            {
                break;
            }

            yield return null;
        }

        GameController.Instance.SetGameState(GameState.Playing);
        StartMeasuring();
    }
}
