﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameModeType
{
    None = 0, MaxScoreReached = 1, Balls3 = 2,
}

public enum GameState
{
    None = 0, Start = 1, Playing = 2, Paused = 3, End = 4
}

public enum MusicType
{
    None = 0, MainMenu = 1, MainLevel = 2, End = 3
}

public enum RewardType
{
    None = 0, Balls = 1, DoubleCurrency = 2, RandomReward = 3
}

public enum ShowAdResult
{
    Failed = 0,
    Skipped = 1,
    Finished = 2,
}

public enum AdsType
{
    Video,
    RewardedVideo,
}
