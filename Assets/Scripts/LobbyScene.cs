﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyScene : MonoBehaviour
{
    public Text _Progress = null;

    private void Start()
    {
        StartCoroutine(LoadMainMenu());
    }

    private IEnumerator LoadMainMenu()
    {
        yield return new WaitForSeconds(1);

        float time = 2f;
        float currentTime = 0f;
        while (true)
        {
            currentTime += Time.deltaTime;

            _Progress.text = ((currentTime / time) * 100f).ToString("F0");

            if (currentTime > time)
            {
                break;
            }

            yield return null;
        }

        GameRoot.Get().LoadMainMenuFromLobby();
    }
}
