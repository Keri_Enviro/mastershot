﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRoot : MonoBehaviour
{
    private static GameRoot _Instance = null;

    public static GameRoot Get()
    {
        return _Instance;
    }

    [SerializeField] private SceneController _SceneController = null;
    public SceneController SceneController
    {
        get { return _SceneController; }
    }

    [SerializeField] private GameConfig _GameConfig = null;
    public GameConfig GameConfig
    {
        get { return _GameConfig; }
    }

    [SerializeField] private LevelManager _LevelManager = null;
    public LevelManager LevelManager
    {
        get { return _LevelManager; }
    }

    [SerializeField] private AudioController _AudioController = null;
    public AudioController AudioController
    {
        get { return _AudioController; }
    }

    [SerializeField] private AdsController _AdsController = null;
    public AdsController AdsController
    {
        get { return _AdsController; }
    }

    private PlayerData _PlayerData = new PlayerData();
    public PlayerData PlayerData
    {
        get { return _PlayerData; }
    }

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Debug.LogError("Vyrobil si dalsi game root!!!");
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadMainMenuFromLobby()
    {
        _SceneController.LoadMainMenuFromLobby();
    }

    public void LoadMainMenuFromMainLevel(int pLevel)
    {
        string level = _SceneController.GetLevel(pLevel);
        _SceneController.LoadMainMenuFromMainLevel(level);
    }

    public void LoadMainLevelFromMainMenu(int pLevel)
    {
        string level = _SceneController.GetLevel(pLevel);
        _SceneController.LoadMainLevelFromMainMenu(level);
    }
}
