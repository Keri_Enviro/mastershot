﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private const string LOBBY = "Lobby";
    private const string FADE_SCENE = "FadeScene";
    private const string MAIN_MENU = "MainMenu";

    private const string LEVEL_1_10 = "Level01-10";
    private const string LEVEL_11_20 = "Level11-20";
    private const string LEVEL_21_30 = "Level21-30";

    public Action _OnStartLevelLoading;
    public Action _OnLevelLoaded;
    public Action _OnStartLevelUnloading;
    public Action _OnLevelUnloaded;
    public Action<float> _OnLevelLoadingProgressChanged;

    public void LoadMainMenuFromLobby()
    {
        StartCoroutine(LoadLevelAsync(LOBBY, MAIN_MENU));
    }

    public void LoadMainLevelFromMainMenu(string pLevel)
    {
        StartCoroutine(LoadLevelAsync(MAIN_MENU, pLevel));
    }

    public void LoadMainMenuFromMainLevel(string pLevel)
    {
        StartCoroutine(LoadLevelAsync(pLevel, MAIN_MENU));
    }

    public string GetLevel(int pLevel)
    {
        string level = "";

        if (pLevel < 11)
        {
            level = LEVEL_1_10;
        }
        else if(pLevel < 21)
        {
            level = LEVEL_11_20;
        }
        else if (pLevel < 31)
        {
            level = LEVEL_21_30;
        }

        return level;
    }

    private IEnumerator LoadLevelAsync(string pUnloadScene, string pLoadScene)
    {
        AsyncOperation asyncLoadOp = SceneManager.LoadSceneAsync(FADE_SCENE, LoadSceneMode.Additive);

        while (!asyncLoadOp.isDone)
        {
            yield return null;
        }

        yield return FadeScene.Get().OpenFadeScene();

        if (pUnloadScene != "")
        {
            AsyncOperation asyncUnloadOp = SceneManager.UnloadSceneAsync(pUnloadScene);

            while (!asyncUnloadOp.isDone)
            {
                yield return null;
            }
        }

        yield return new WaitForSeconds(0.5f);

        AsyncOperation asyncOp = SceneManager.LoadSceneAsync(pLoadScene, LoadSceneMode.Additive);

        while (!asyncOp.isDone)
        {
            if (_OnLevelLoadingProgressChanged != null)
            {
                _OnLevelLoadingProgressChanged(asyncOp.progress);
            }

            yield return null;
        }

        if (_OnLevelLoadingProgressChanged != null)
        {
            _OnLevelLoadingProgressChanged(1.0f);
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByName(pLoadScene));

        yield return FadeScene.Get().CloseFadeScene(FadeScene._EndTime);
        SceneManager.UnloadSceneAsync(FADE_SCENE);

        if (_OnLevelLoaded != null)
        {
            _OnLevelLoaded();
        }
    }
}
