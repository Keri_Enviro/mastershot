﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsController : MonoBehaviour
{
    public Action<ShowResult, RewardType> OnAdsFinished;

    private const string REWARDED_ADS = "rewardedVideo";
    private const string VIDEO_ADS = "video";

    private RewardType _CurrentRewardType = RewardType.None;

    private void Awake()
    {
        Advertisement.Initialize("2901533");
    }

    public bool IsAdsShowing()
    {
#if UNITY_ANDROID
        return Advertisement.isShowing;
#else
        return false;
#endif
    }

    public bool IsAdsReady(AdsType pAdsType)
    {
        string adsString = REWARDED_ADS;

        switch (pAdsType)
        {
            case AdsType.Video:
                adsString = VIDEO_ADS;
                break;
            case AdsType.RewardedVideo:
                adsString = REWARDED_ADS;
                break;
        }

#if UNITY_ANDROID
        return Advertisement.IsReady(adsString);
#else
        return true;
#endif
    }

    public void ShowAd(AdsType pAdsType, RewardType pRewardType)
    {
        _CurrentRewardType = pRewardType;
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        string adsString = REWARDED_ADS;

        switch (pAdsType)
        {
            case AdsType.Video:
                adsString = VIDEO_ADS;
                break;
            case AdsType.RewardedVideo:
                adsString = REWARDED_ADS;
                break;
        }

        StartCoroutine(IsShowingAd());

        Advertisement.Show(adsString, options);
    }

    private void HandleShowResult(ShowResult pResult)
    {
        switch (pResult)
        {
            case ShowResult.Finished:
                Debug.Log("Video completed - Offer a reward to the player");
                break;
            case ShowResult.Failed:
                Debug.LogError("Video failed to show");
                break;
            case ShowResult.Skipped:
                Debug.LogWarning("Video was skipped - Do NOT reward the player");
                break;
        }

        if (OnAdsFinished != null)
            OnAdsFinished(pResult, _CurrentRewardType);
    }


    private IEnumerator IsShowingAd()
    {
        float timeScale = Time.timeScale;
        Time.timeScale = 0f;

        while (Advertisement.isShowing)
        {
            yield return null;
        }

        Time.timeScale = timeScale;
    }
}
