﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController
{
    public Action OnMaxScoreReached;

    private int _Score = 0;
    private int _MaxScore = 0;

    public void Init()
    {
        _Score = 0;
        _MaxScore = 0;
    }

    public void IncreaseScore()
    {
        _Score++;

        if (_Score == _MaxScore)
        {
            if (OnMaxScoreReached != null)
            {
                OnMaxScoreReached();
            }
        }
    }

    public void IncreaseMaxScore()
    {
        _MaxScore++;
    }
}
