﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHolder : MonoBehaviour
{
    [SerializeField]
    private Camera _Camera = null;

    public Transform _Target = null;

    private void LateUpdate()
    {
        if (_Target == null) return;

        _Camera.transform.LookAt(_Target);
    }

    public void SetTarget(Transform pTarget)
    {
        _Target = pTarget;
    }
}
