﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdButton : MonoBehaviour
{
    public RewardType _RewardType = RewardType.None;

    public void ShowAd()
    {
        if (!GameRoot.Get().AdsController.IsAdsReady(AdsType.RewardedVideo)) return;

        GameRoot.Get().AdsController.ShowAd(AdsType.RewardedVideo, _RewardType);
    }
}
