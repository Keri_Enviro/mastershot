﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    public GameScreen   _GameScreen = null;
    public EndScreen    _EndScreen = null;

    public void Init()
    {
        GameController.Instance.ScoreController.OnMaxScoreReached += OnMaxScoreReached;
        _GameScreen.Init();
        _EndScreen.CloseScreen();
    }

    public void OpenGameScreen()
    {
        _GameScreen.OpenScreen();
        _EndScreen.CloseScreen();
    }

    public void OpenEndScreen()
    {
        _GameScreen.CloseScreen();
        _EndScreen.OpenScreen();
    }

    private void OnMaxScoreReached()
    {
        OpenEndScreen();
    }
}
