﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScreen : ScreenBase
{
    public override void OpenScreen()
    {
        switch (GameController.Instance.GameState)
        {
            case GameState.Playing:
                GameController.Instance.SetGameState(GameState.Paused);
                GameController.Instance.TimeController.StopMeasuring();
                GameRoot.Get().AudioController.SetSnapshot(GameState.Paused);
                break;
            case GameState.End:
                GameController.Instance.TimeController.StopMeasuring();
                GameRoot.Get().AudioController.PlayMusic(MusicType.End);
                break;
        }

        base.OpenScreen();
    }

    public override void CloseScreen()
    {
        switch (GameController.Instance.GameState)
        {
            case GameState.Paused:
                GameController.Instance.SetGameState(GameState.Playing);
                GameController.Instance.TimeController.StartMeasuring();
                break;
        }

        GameRoot.Get().AudioController.SetSnapshot(GameState.Playing);

        base.CloseScreen();
    }

    public void Resume()
    {
        GameController.Instance.ScreenController.OpenGameScreen();
    }

    public void BackToMainMenu()
    {
        GameRoot.Get().LoadMainMenuFromMainLevel(1);
    }
}
