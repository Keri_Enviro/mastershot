﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelButton : MonoBehaviour, IPointerClickHandler
{
    public Color    _NormalColor = Color.white;
    public Color    _SelectedColor = Color.green;
    public Image    _Image = null;
    public Text     _TextLevel = null;

    public int _Level = 0;

    public void Init(int pLevel)
    {
        _Level = pLevel;
        _TextLevel.text = pLevel.ToString();
    }

    public void SetSelected(bool pSelected)
    {
        _Image.color = pSelected ? _SelectedColor : _NormalColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        MainMenuController.Instance.SetCurrentLevel(_Level, this);
    }
}
