﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : ScreenBase
{
    public Text _TimeValue = null;
    public Text _Countdown = null;

    public void Init()
    {
        GameController.Instance.TimeController.OnTimeChanged += OnTimeChanged;
        GameController.Instance.TimeController.OnCountdownChanged += OnCountdownChanged;
        GameController.Instance.OnGameStateChanged += OnGameStateChanged;
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameController.Instance.ScreenController.OpenEndScreen();
        }
    }
#endif

    private void OnTimeChanged(float pTime)
    {
        _TimeValue.text = pTime.ToString("F1");
    }

    private void OnCountdownChanged(float pProgress)
    {
        int seconds = Mathf.CeilToInt(pProgress);
        _Countdown.text = seconds.ToString();
    }

    private void OnGameStateChanged(GameState pState)
    {
        switch (pState)
        {
            case GameState.Playing:
                _Countdown.text = "";
                break;
        }
    }
}
