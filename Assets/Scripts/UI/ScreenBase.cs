﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScreenBase : MonoBehaviour
{
    public virtual void OpenScreen()
    {
        gameObject.SetActive(true);
    }

    public virtual void CloseScreen()
    {
        gameObject.SetActive(false);
    }
}
