﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance = null;

    public Action<GameState> OnGameStateChanged;

    [SerializeField] private Bullet             _BulletPrefab = null;
    [SerializeField] private CameraRotator      _CameraRotator = null;
    [SerializeField] private Image              _ForceImage = null;
    [SerializeField] private ScreenController   _ScreenController = null;
    [SerializeField] private Target             _TargetPrefab = null;
    [SerializeField] private Transform          _TargetParent = null;

    public ScreenController ScreenController
    {
        get { return _ScreenController; }
    }

    private TimeController _TimeController = null;
    public TimeController TimeController
    {
        get { return _TimeController; }
    }

    private ScoreController _ScoreController = null;
    public ScoreController ScoreController
    {
        get { return _ScoreController; }
    }

    public GameState GameState
    {
        get;
        private set;
    }

    private Bullet  _CurrentBullet = null;
    private float   _CurrentForceTime = 0f;
    private int     _BulletCount = 0;
    private bool    _IsShot = false;
    private GameModeType _GameModeType = GameModeType.None;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Vytvoril si druhy GameController!!!");
        }

        _GameModeType = GameModeType.MaxScoreReached;

        _TimeController = new TimeController();
        _ScoreController = new ScoreController();
        _ScoreController.OnMaxScoreReached += OnMaxScoreReached;

        GameState = GameState.Start;

        _TimeController.StartCountdown();

        CreateTargets();

        GameRoot.Get().AudioController.PlayMusic(MusicType.MainLevel);
        GameRoot.Get().AudioController.SetSnapshot(GameState.Start);
        GameRoot.Get().AudioController.SetSoundEffectsVolume(0f);
    }

    private void Start()
    {
        _ScreenController.Init();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _CurrentForceTime += Time.deltaTime;
        }

        _ForceImage.fillAmount = _CurrentForceTime;

        //Shot
        if (Input.GetMouseButtonUp(0))
        {
            if (_CurrentBullet != null)
            {
                _CurrentBullet.Shot(-_CameraRotator.transform.right, _CurrentForceTime);
                _CurrentForceTime = 0f;

                if (!_IsShot)
                {
                    _TimeController.StartMeasuring();
                    _IsShot = true;
                }
            }
        }

        //Reload
        if (Input.GetMouseButtonDown(1))
        {
            GameObject go = Instantiate(_BulletPrefab.gameObject, new Vector3(12, 1, 0), Quaternion.identity);
            _CurrentBullet = go.GetComponent<Bullet>();
            _BulletCount++;
        }

        _TimeController.UpdateTC();
    }

    private void CreateTargets()
    {
        GameConfig config = GameRoot.Get().GameConfig;
        LevelPositions levelPos = config._LevelPositions[GameRoot.Get().LevelManager._CurrentLevel - 1];
        int length = levelPos._Positions.Length;
        for (int i = 0; i < length; i++)
        {
            GameObject go = Instantiate(_TargetPrefab.gameObject, levelPos._Positions[i], Quaternion.identity,
                _TargetParent);
        }
    }

    public void SetGameState(GameState pState)
    {
        GameState = pState;
        if (OnGameStateChanged != null)
        {
            OnGameStateChanged(GameState);
        }
    }

    private void OnMaxScoreReached()
    {
        switch (_GameModeType)
        {
            case GameModeType.MaxScoreReached:
                _TimeController.StopMeasuring();
                GameState = GameState.End;
                break;
        }
    }
}
