﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance = null;

    public LevelButton  _LevelButtonPrefab = null;
    public Transform    _LevelButtonsParent = null;

    private int         _SelectedLevel = 1;
    private LevelButton _SelectedButton = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Vytvoril si druhy MainMenuController!!!");
        }

        GameConfig config = GameRoot.Get().GameConfig;
        int length = config._LevelPositions.Length;
        for (int i = 0; i < length; i++)
        {
            GameObject go = Instantiate(_LevelButtonPrefab.gameObject, _LevelButtonsParent);
            LevelButton button = go.GetComponent<LevelButton>();
            button.Init(i + 1);
            button.SetSelected(i == 0);
        }

        GameRoot.Get().AudioController.PlayMusic(MusicType.MainMenu);
        GameRoot.Get().AudioController.SetSnapshot(GameState.Start);
        GameRoot.Get().AudioController.SetSoundEffectsVolume(-80f);
    }

    public void SetCurrentLevel(int pLevel, LevelButton pButton)
    {
        if (_SelectedButton != null)
        {
            _SelectedButton.SetSelected(false);
        }

        _SelectedLevel = pLevel;
        _SelectedButton = pButton;
        _SelectedButton.SetSelected(true);
    }

    public void LoadLevel()
    {
        GameRoot.Get().LevelManager._CurrentLevel = _SelectedLevel;
        GameRoot.Get().LoadMainLevelFromMainMenu(_SelectedLevel);
    }
}
